from collections import defaultdict
from typing import Callable, Awaitable, Any

from aaambos.core.communication.topic import Topic, TopicId
from aaambos.std.communications.utils import IUEvent
from ipaacar.components import IU
from ipaacar.handler import IUCallbackHandlerInterface

# import logging, sys
# logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

class NewIUHandler(IUCallbackHandlerInterface):

    def __init__(self, encoder, ob_id, encoder_with_wrapper=None, update_handler: IUCallbackHandlerInterface = None):
        self.is_new_iu_handler = bool(update_handler)
        self.encoder_with_wrapper = encoder_with_wrapper
        self.ob_id = ob_id
        self.encoder = encoder
        self.callbacks: dict[TopicId, list[Callable[[Topic, Any, 'IUEvent', 'IU', bool], Awaitable[None]]]] = defaultdict(list)
        self.topic_id_to_topic = defaultdict()
        self.wrapper = defaultdict()
        self.update_handler = update_handler
        # self.last_called = time.time()

    async def process_iu_callback(self, iu: IU):
        topic_id = await iu.get_category()
        if self.topic_id_to_topic[topic_id]:
            payload_str = str.encode(await iu.get_payload())
            # print(payload_str, str(iu), self.is_new_iu_handler, time.time() - self.last_called, self.ob_id, await iu.get_owner_buffer_uid())
            # self.last_called = time.time()
            # getting an iu updated in another module is very slow ~40ms vs 1ms if it is the module of change
            if self.encoder_with_wrapper and self.wrapper[topic_id] is not ...:
                payload = self.encoder(payload_str, **{self.encoder_with_wrapper: self.wrapper[topic_id]})
            else:
                payload = self.wrapper[topic_id](**self.encoder(payload_str)) if self.wrapper[topic_id] is not ... else self.encoder(payload_str)
            iu_event = IUEvent.NEW if self.is_new_iu_handler else (IUEvent.COMMITTED if await iu.is_committed() else IUEvent.UPDATED)
            owner_buffer_uid = await iu.get_owner_buffer_uid()
            if self.is_new_iu_handler:
                await iu.add_callback(self.update_handler)
                if self.ob_id == owner_buffer_uid:
                    # if is new_handler and own iu then do not call the callback
                    # maybe delete
                    return
            
            for callback in self.callbacks[topic_id]:
                await callback(self.topic_id_to_topic[topic_id], payload, iu_event, iu, owner_buffer_uid==self.ob_id)

    def add_callback(self, topic: Topic, callback: Callable[[Topic, Any, 'IUEvent', 'IU', bool], Awaitable[None]], wrapper=...):
        self.callbacks[topic.id()].append(callback)
        self.topic_id_to_topic[topic.id()] = topic
        self.wrapper[topic.id()] = wrapper
