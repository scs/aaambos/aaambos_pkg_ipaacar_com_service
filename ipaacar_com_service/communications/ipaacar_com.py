import asyncio
from collections import defaultdict
from typing import Callable, Any, Awaitable, Type

import msgspec.json
from aaambos.core.communication.promise import CommunicationPromise
from aaambos.core.communication.service import CommunicationService, CommunicationServiceInfo, \
    to_attribute_dict
from aaambos.core.communication.setup import CommunicationSetup
from aaambos.core.communication.topic import Topic, TopicId
from aaambos.core.configuration.module_config import ModuleCommunicationInfo, ModuleConfig
from aaambos.core.execution.concurrency import ConcurrencyLevel
from aaambos.std.communications.attributes import MsgTopicSendingAttribute, MsgPayloadWrapperCallbackReceivingAttribute, \
    PayloadWrapperAttribute, RunTimeReceivingNewTopicsAttribute, RunTimeSendingNewTopicsAttribute, \
    IncrementalUnitTopicWrapperAttribute, IncrementalUnitAccessAttribute
from aaambos.std.communications.categories import IPAACAR_IU_TYPE, INCREMENTAL_TYPE, MSGSPEC_STRUCT_TYPE
from aaambos.std.communications.utils import is_iu_promise
from ipaacar.components import create_mqtt_pair, OutputBuffer, InputBuffer, IU
from ipaacar.handler import MessageCallbackHandlerInterface
from loguru import logger
from msgspec import Struct

from ipaacar_com_service.communications.ipaacar_iu_handler import NewIUHandler
from ipaacar_com_service.communications.utils import IPAACAR_ADDRESS_KEY, IPAACAR_DEFAULT_ADDRESS

IPAACAR_COM = "ipaacar"
IPAACAR_COM_TYPE = "ipaacar_com_type"
IPAACAR_IMPORT = "ipaacar_com_service.communications.ipaacar_com.IPAACARService"

TOPIC_KEY = "key"
PAYLOAD_KEY = "payload"


class Msg(Struct):
    topic: str
    payload: Any


class IPAACARService(CommunicationService,
                     MessageCallbackHandlerInterface,
                     MsgTopicSendingAttribute,
                     MsgPayloadWrapperCallbackReceivingAttribute,
                     PayloadWrapperAttribute,
                     RunTimeReceivingNewTopicsAttribute,
                     RunTimeSendingNewTopicsAttribute,
                     IncrementalUnitTopicWrapperAttribute,
                     IncrementalUnitAccessAttribute):
    ib: InputBuffer | None
    ob: OutputBuffer | None

    def __init__(self, config: ModuleCommunicationInfo, mqtt_pair_info: tuple[str, str, str, str],
                 in_promises: list[CommunicationPromise], out_promises: list[CommunicationPromise]):
        super().__init__(config)
        self.mqtt_pair_info = mqtt_pair_info
        self.ib, self.ob = None, None
        self.in_promises = in_promises
        self.out_promises = out_promises
        self.topic_to_callback_and_wrapper: dict[
            TopicId, list[tuple[Callable[[Topic, Any], Awaitable[None]], Type[Any]]]] = defaultdict(list)
        self.topics_listening: dict[TopicId, Topic] = {}
        self.topics_sending: dict[TopicId, Topic] = {}
        self.iu_handler: tuple[NewIUHandler, NewIUHandler] | None = None
        self.ib_uid, self.ob_uid = mqtt_pair_info[:2]

    async def initialize(self):
        self.ib, self.ob = await create_mqtt_pair(*self.mqtt_pair_info)
        for in_promise in self.in_promises:
            self.topics_listening[in_promise.settings.topic.id()] = in_promise.settings.topic
            logger.info(f"ib listen to {in_promise.settings.topic.id()}")
            await self.ib.listen_to_category(in_promise.settings.topic.id())
            if is_iu_promise(in_promise):
                if not self.iu_handler:
                    self.init_iu_handler()
                    await self.ib.on_new_iu(self.iu_handler[0])
        await self.ib.on_new_message(self)

        for out_promise in self.out_promises:
            self.topics_sending[out_promise.settings.topic.id()] = out_promise.settings.topic

    async def send(self, topic, msg):
        await self.ob.send_message(topic.id(),
                                   msgspec.json.encode(Msg(topic=topic.id(), payload=msg)).decode("utf-8"))

    async def register_callback(self, topic: Topic, callback: Callable[[Topic, Any], Awaitable[None]],
                                wrapper: Type[Any] = ..., *args, **kwargs):
        if callback not in self.topic_to_callback_and_wrapper[topic.id()]:
            logger.info(f"Add callback {topic.id()}")
            msgspec_struct = msgspec.defstruct("Msg", [("topic", TopicId), ("payload", wrapper)])
            self.topic_to_callback_and_wrapper[topic.id()].append((callback, msgspec_struct))

    async def unregister_callback(self, topic: Topic, callback: Callable):
        if topic.id() in self.topic_to_callback_and_wrapper:
            callback_idxs = [i for i, c in enumerate(self.topic_to_callback_and_wrapper[topic.id()]) if
                             c[0] == callback]
            for i in reversed(callback_idxs):
                self.topic_to_callback_and_wrapper[topic.id()].pop(i)
            if not self.topic_to_callback_and_wrapper[topic.id()]:
                del self.topic_to_callback_and_wrapper[topic.id()]

    async def add_input_topic(self, topic: Topic, callback: Callable[[Topic, Any], Awaitable[None]] | Callable[[Topic, Any, 'IUEvent', 'IU', bool], Awaitable[None]] = ...,
                              wrapper: Any = ...):
        is_subscribed = topic.id() in self.topic_to_callback_and_wrapper
        if not is_subscribed:
            msgspec_struct = msgspec.defstruct("Msg", [("topic", TopicId), ("payload", wrapper)])
            self.topic_to_callback_and_wrapper[topic.id()].append((callback, msgspec_struct))
            logger.info(f"now also listen to {topic.id()}")
            self.topics_listening[topic.id()] = topic
            await self.ib.listen_to_category(topic.id())

    async def add_output_topic(self, topic: Topic):
        self.topics_sending[topic.id()] = topic

    async def process_message_callback(self, msg: str):
        msg_dict = msgspec.json.decode(str.encode(msg), type=Msg)
        # assert PAYLOAD_KEY in msg_dict and TOPIC_KEY in msg_dict, f"Requires payload key and topic key in received msg {msg!r}"
        if self.topic_to_callback_and_wrapper[msg_dict.topic]:  # [msg_dict[TOPIC_KEY]]:
            for callback, wrapper in self.topic_to_callback_and_wrapper[msg_dict.topic]:  # [TOPIC_KEY]]:
                # TODO check if msgspec encode can type check payload part in a second run or not encode dict
                if wrapper is not ...:
                    # TODO based on promise/unit ?
                    converted_msg = msgspec.json.decode(str.encode(msg), type=wrapper).payload
                else:
                    converted_msg = msg_dict.payload  # [PAYLOAD_KEY]
                await callback(self.topics_listening[msg_dict.topic], converted_msg)
                
    def check_wrapping(self, payload, wrapper: Type[Any]) -> bool:
        pass

    async def create_and_send_iu(self, topic: Topic, payload):
        iu = await self.ob.create_new_iu(topic.id(), msgspec.json.encode(payload).decode("utf-8"))
        # await iu.add_callback(self.iu_handler[1])
        await iu.get_uid()
        return iu

    async def register_callback_iu(self, topic: Topic,
                                   callback: Callable[[Topic, Any, 'IUEvent', 'IU', bool], Awaitable[None]],
                                   wrapper: Type[Any] = ..., *args, **kwargs):
        await self.add_input_topic(topic, callback, wrapper)
        for iu_handler in self.iu_handler:
            iu_handler.add_callback(topic, callback, wrapper)

    async def get_all_ius(self) -> list['IU']:
        return await self.ib.get_all_ius()

    async def get_iu_by_id(self, uid: str) -> 'IU':
        return await self.ib.get_iu_by_id(uid)

    async def set_payload_iu(self, iu: IU, payload):
        await iu.set_payload(msgspec.json.encode(payload).decode("utf-8"))

    def init_iu_handler(self):
        # TODO encoder by config?
        update_handler = NewIUHandler(msgspec.json.decode, self.ob_uid, "type")
        self.iu_handler = (
            NewIUHandler(msgspec.json.decode, self.ob_uid, "type", update_handler),
            update_handler
        )


class IPAACARSetup(CommunicationSetup):
    def before_arch_start(self):
        pass

    def before_module_start(self, module_config: ModuleConfig) -> CommunicationService:
        # maybe convert to uuids?
        instance_name = module_config.general_run_config.get_instance_id()
        self.ib_uid = f"IN_{instance_name}_{module_config.name}_{IPAACAR_COM.upper()}_IN"
        self.ob_uid = f"OUT_{instance_name}_{module_config.name}_{IPAACAR_COM.upper()}_OUT"
        component_name = IPAACAR_COM
        address = module_config.general_run_config.plus[IPAACAR_ADDRESS_KEY]
        in_proms, out_proms = self.communication_graph.get_in_out_promises_per_module(module_config.name,
                                                                                      com_service_name=IPAACAR_COM)
        return IPAACARService(module_config.com_info, (self.ib_uid, self.ob_uid, component_name, address), in_proms,
                              out_proms)

    @classmethod
    def run_config_general_plus_keys_and_defaults(cls) -> dict[str, Any]:
        return {IPAACAR_ADDRESS_KEY: IPAACAR_DEFAULT_ADDRESS}


IPAACARInfo = CommunicationServiceInfo(name=IPAACAR_COM,
                                       import_path=IPAACAR_IMPORT,
                                       attributes=to_attribute_dict([
                                           MsgTopicSendingAttribute,
                                           MsgPayloadWrapperCallbackReceivingAttribute,
                                           PayloadWrapperAttribute,
                                           RunTimeReceivingNewTopicsAttribute,
                                           RunTimeSendingNewTopicsAttribute,
                                           IncrementalUnitTopicWrapperAttribute,
                                           IncrementalUnitAccessAttribute,
                                       ]),
                                       extra_categories=[IPAACAR_COM_TYPE, IPAACAR_IU_TYPE, INCREMENTAL_TYPE,
                                                         ConcurrencyLevel.MultiProcessing.name, MSGSPEC_STRUCT_TYPE],
                                       setup=IPAACARSetup)


async def print_iu(iu: IU):
    uid, owner, component, links, category, payload = await asyncio.gather(iu.get_uid(), iu.get_owner_buffer_uid(),
                                                                           iu.get_component_name(), iu.get_links(),
                                                                           iu.get_category(), iu.get_payload())
    print(f"""IU( 
    {uid=}
    {category=}
    {owner=}
    {component=}
    {links=}
    {payload=}
)""")
