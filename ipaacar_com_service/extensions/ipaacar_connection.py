import json
from typing import Type, Callable, Any, Awaitable

import msgspec
from aaambos.core.communication.topic import Topic
from aaambos.core.module.base import Module
from ipaacar.components import create_mqtt_pair
from ipaacar.handler import MessageCallbackHandlerInterface
from semantic_version import Version, SimpleSpec

from aaambos.core.module.extension import Extension, ExtensionSetup
from aaambos.core.module.feature import ExtensionFeature

IPAACAR_CONNECTION_EXTENSION = "IpaacarConnectionExtension"


# TODO IUs
# TODO multiple mqtt pairs and topics

class MessageHandler(MessageCallbackHandlerInterface):

    def __init__(self, callback: Callable[[Any], Awaitable], msgspec_wrapper = None, topic_replacement: Any = None):
        self.callback = callback
        self.msgspec_wrapper = msgspec_wrapper
        self.topic_replacement = topic_replacement

    async def process_message_callback(self, message: str):
        if self.msgspec_wrapper is None:
            c = json.loads(message)
        else:
            c = msgspec.json.decode(str.encode(message), type=self.msgspec_wrapper)
        await self.callback(self.topic_replacement, c)


class IpaacarConnectionExtension(Extension):
    """Connect to external mqtt broker and listen to one topic."""
    name = IPAACAR_CONNECTION_EXTENSION
    ib, ob = None, None
    message_handler = None

    async def before_module_initialize(self, module: Module):
        self.module: Module = module

    async def after_module_initialize(self, module):
        pass

    async def set_mqtt_url(self, url: str, group: str, ib_name: str = None, ob_name: str = None):
        if ib_name is None:
            ib_name = self.module.config.general_run_config.get_instance_id() + "-" + self.module.name + "-IpaacarConnectionExtensionIB"
        if ob_name is None:
            ob_name = self.module.config.general_run_config.get_instance_id() + "-" + self.module.name + "-IpaacarConnectionExtensionOB"

        self.ib, self.ob = await create_mqtt_pair(ib_name, ob_name, group, url)

    async def set_listen_topic(self, topic: str, callback, mspspec_wrapper: Any = None, topic_replacement: Any = None):
        if None in [self.ib, self.ob]:
            raise Exception("You need first to set the mqtt url of the IpaacarConnectionExtension before you can listen to a topic.")

        if self.message_handler is not None:
            raise Exception("Already listen to one topic. The old callback would receive the messages from the other topic, too.")

        self.message_handler = MessageHandler(callback=callback, msgspec_wrapper=mspspec_wrapper, topic_replacement=topic_replacement)
        await self.ib.listen_to_category(topic)
        await self.ib.on_new_message(self.message_handler)

    async def send(self, topic, msg):
        if None in [self.ib, self.ob]:
            raise Exception("You need first to set the mqtt url of the IpaacarConnectionExtension before you can send a message.")
        if isinstance(topic, Topic):
            topic = topic.id()
        if isinstance(msg, (dict, str)):
            await self.ob.send_message(topic, json.dumps(msg))
        else:  # is instance Struct
            await self.ob.send_message(topic, msgspec.json.encode(msg).decode("utf-8"))


    @classmethod
    def get_extension_setup(cls) -> Type[ExtensionSetup]:
        return IpaacarConnectionExtensionSetup


class IpaacarConnectionExtensionSetup(ExtensionSetup):

    def before_module_start(self, module_config) -> Extension:
        return IpaacarConnectionExtension()


IpaacarConnectionExtensionFeature = ExtensionFeature(
    name=IPAACAR_CONNECTION_EXTENSION,
    version=Version("0.1.0"),
    requirements=[],
    version_compatibility=SimpleSpec(">=0.1.0"),
    extension_setup_class=IpaacarConnectionExtensionSetup,
    as_kwarg=True,
    kwarg_alias="ipaacar_connection",
)



