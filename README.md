# ipaacar_com_service

A communication service for the message system IPAACAR (Incremental Processing Architecture for Artifical Conversational Agents, implemented in Rust) in the aaambos framework.


# req:
- ipaacar [gitlab/ipaacaR](https://gitlab.ub.uni-bielefeld.de/scs/ipaacar)
- pprint